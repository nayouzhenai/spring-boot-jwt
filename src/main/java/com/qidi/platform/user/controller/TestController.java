package com.qidi.platform.user.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 包    名 ：com.qidi.platform.user.controller
 * 文 件 名 : TestController
 * 描    述 : TODO
 * 作    者 ：Administrator超哥
 * 创建时间 ：2017/12/19 0019 下午 13:20
 * 版    本 ：1.0
 */
@RestController
public class TestController {


    @PostMapping(value = "/hello")
    public String hello(String username, String password) {
        return "hello";
    }
}
