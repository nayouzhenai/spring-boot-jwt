package com.qidi.platform.user.service;

import com.qidi.platform.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * 包    名 ：com.qidi.platform.user.service
 * 文 件 名 : IUserRepository
 * 描    述 : TODO
 * 作    者 ：Administrator超哥
 * 创建时间 ：2017/12/18 0018 下午 15:00
 * 版    本 ：1.0
 */
@Repository
public interface IUserRepository extends JpaRepository<User,Long> {

    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return 用户信息
     */
    User findByUsername(String username);
}
