package com.qidi.platform.user.entity;

import javax.persistence.*;
import java.util.List;

/**
 * 包    名 ：com.qidi.platform.user.entity
 * 文 件 名 : User
 * 描    述 : TODO  用户模型
 * 作    者 ：Administrator超哥
 * 创建时间 ：2017/12/18 0018 下午 14:59
 * 版    本 ：1.0
 */
@Entity
@Table(name = "t_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long guid;
    @Column
    private String username;
    @Column
    private String password;

   @Transient
    private List<String> roles;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }
}
